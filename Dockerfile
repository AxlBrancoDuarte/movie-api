FROM gradle:jdk17 AS gradle
WORKDIR /app
COPY . .
RUN gradle bootJar

FROM openjdk:17 as runtime
WORKDIR /app

ENV SPRING_PROFILE "prod"
ENV JPA_DDL "none"
ENV INIT_SQL "never"
ENV SHOW_SQL "false"
ENV SEED_SCHEMA "false"

COPY --from=gradle /app/build/libs/*.jar /app/app.jar
RUN chown -R 1000:1000 /app
USER 1000:1000

ENTRYPOINT ["java", "-jar", "app.jar"]
