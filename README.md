# Automated testing with Gradle and JUnit
[![pipeline status](https://gitlab.com/AxlBrancoDuarte/Movie-API/badges/main/pipeline.svg)](https://gitlab.com/AxlBrancoDuarte/Movie-API/-/commits/main)

<div id="top"></div>

<!-- PROJECT LOGO -->
<br />
<div align="center">
  <a href="https://github.com/github_username/repo_name">
    <img src="spring.png" alt="Logo" width="80" height="80">
  </a>

<h3 align="center">Web API and database with Spring</h3>

  <p align="center">
    Create a PostgreSQL database using Hibernate and expose it through a deployed Web API.
  </p>
</div>



<!-- TABLE OF CONTENTS -->

  <summary>Table of Contents</summary>
  <ol>
    <li>
      <a href="#about-the-project">About The Project</a>
      <ul>
        <li><a href="#built-with">Built With</a></li>
      </ul>
    </li>
    <li><a href="#license">License</a></li>
    <li><a href="#contact">Contact</a></li>
  </ol>




<!-- ABOUT THE PROJECT -->
## About The Project

 

This is my third Java Assignment Project for the Noroff Java Bootcamp.
Where we were tasked with creating a PostgreSQL database about movies using Hibernate and expose it through a deployed Web API were users can then manipulate the data.

Screenshot showing the swagger crud functionality. [Swagger](https://docker-movie-api.herokuapp.com/swagger-ui/index.html#/)

<a href="https://github.com/github_username/repo_name">
<img src="swagger.png" alt="app">
</a>

<p align="right">(<a href="#top">back to top</a>)</p>



### Built With

* [Java](https://www.java.com/nl/)
* [Spring](https://spring.io/)
* [Gradle](https://gradle.org/)
* [Docker](https://www.docker.com/)
* [Heroku](https://id.heroku.com/login)
* [Swagger](https://swagger.io/)
* [Intellij](https://www.jetbrains.com/idea/)


<p align="right">(<a href="#top">back to top</a>)</p>





<!-- ROADMAP -->






<!-- CONTRIBUTING -->






<!-- LICENSE -->
## License

Distributed under the MIT License. See `LICENSE.txt` for more information.

<p align="right">(<a href="#top">back to top</a>)</p>



<!-- CONTACT -->
## Contact

Axl Branco Duarte - axl.nl@hotmail.com

<p align="right">(<a href="#top">back to top</a>)</p>



<!-- ACKNOWLEDGMENTS -->





<!-- MARKDOWN LINKS & IMAGES -->
<!-- https://www.markdownguide.org/basic-syntax/#reference-style-links -->
