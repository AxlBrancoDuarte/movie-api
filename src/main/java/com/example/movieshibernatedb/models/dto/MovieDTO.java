package com.example.movieshibernatedb.models.dto;

import lombok.Data;

import java.net.URL;
import java.util.Set;

@Data
public class MovieDTO {
    private int id;
    private String title;
    private String genre;
    private String releaseYear;
    private String director;
    private String poster;
    private String trailer;
    private Integer franchise;
    private Set<Integer> characters;

}
