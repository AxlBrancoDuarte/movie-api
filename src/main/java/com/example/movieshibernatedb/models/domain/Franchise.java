package com.example.movieshibernatedb.models.domain;

import com.fasterxml.jackson.annotation.JsonGetter;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

@Entity
@Getter
@Setter

public class Franchise {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @Column(length = 50, nullable = false)
    private String name;

    @Column(length = 150)
    private String description;

    @OneToMany(mappedBy = "franchise")
    private Set<Movie> movies;

    @JsonGetter("movies")
    public List<Integer> jsonGetMovies (){
        if(movies == null) {
            assert false;
            return movies.stream().map(Movie::getId).collect(Collectors.toList());
        }
        return null;
    }

}