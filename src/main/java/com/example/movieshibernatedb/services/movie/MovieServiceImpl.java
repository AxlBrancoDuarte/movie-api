package com.example.movieshibernatedb.services.movie;

import com.example.movieshibernatedb.exceptions.NotFoundException;
import com.example.movieshibernatedb.models.domain.Character;
import com.example.movieshibernatedb.models.domain.Movie;
import com.example.movieshibernatedb.repositories.CharacterRepository;
import com.example.movieshibernatedb.repositories.MovieRepository;

import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.Collection;
import java.util.HashSet;
import java.util.Set;


@Service
public class MovieServiceImpl implements MovieService{

    private final MovieRepository movieRepository;
    private final CharacterRepository characterRepository;
    public MovieServiceImpl(MovieRepository movieRepository, CharacterRepository characterRepository) {
        this.movieRepository = movieRepository;
        this.characterRepository = characterRepository;
    }

    @Override
    public Movie findById(Integer id) {
        return movieRepository.findById(id).orElseThrow(() -> new NotFoundException("Movie does not exist with ID: " + id));
    }

    @Override
    public Collection<Movie> findAll() {
        return movieRepository.findAll();
    }

    @Override
    public Movie add(Movie entity) {
        return movieRepository.save(entity);
    }

    @Override
    public void update(Movie movie) {
        int id = movie.getId();
        if(!movieRepository.existsById(id)) throw new NotFoundException("Movie does not exist with ID: " + id);
        movieRepository.save(movie);

    }

    @Override
    public void deleteById(Integer id) {
        Movie movie = movieRepository.findById(id).orElseThrow(()-> new NotFoundException("Movie does not exist with ID: " + id));
        movieRepository.delete(movie);
    }

    @Override
    @Transactional
    public Set<Character> findCharactersMovie(Movie movie){
        Set<Integer> charIds = movieRepository.getCharactersIdMovie(movie.getId());
        Set<Character> characters = new HashSet<>();
        for(int id : charIds){
            Character character = characterRepository.findById(id).get();
            characters.add(character);
        }
        return characters;
    }

    @Override
    @Transactional
    public void updateCharactersMovie(int[] characterIds, int movieId){
        Movie movie = movieRepository.findById(movieId).orElseThrow(() -> new NotFoundException("Movie does not exist with ID: " + movieId));
        Set<Character> characters = new HashSet<>();
        movie.setCharacters(null);
        for(int charId: characterIds){
            Character character = characterRepository.findById(charId).orElseThrow(() -> new NotFoundException("Character does not exist with ID: " + charId));
            characters.add(character);
        }
        movie.setCharacters(characters);
    }

}

