package com.example.movieshibernatedb.services.movie;

import com.example.movieshibernatedb.models.domain.Character;
import com.example.movieshibernatedb.models.domain.Movie;
import com.example.movieshibernatedb.services.CrudService;

import java.util.Collection;
import java.util.Set;

public interface MovieService extends CrudService<Movie, Integer> {
    Set<Character> findCharactersMovie(Movie movie);
    void updateCharactersMovie(int[] characterIds, int movieId);

}
