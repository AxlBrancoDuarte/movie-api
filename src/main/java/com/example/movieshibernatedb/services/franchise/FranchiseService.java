package com.example.movieshibernatedb.services.franchise;

import com.example.movieshibernatedb.models.domain.Character;
import com.example.movieshibernatedb.models.domain.Franchise;
import com.example.movieshibernatedb.models.domain.Movie;
import com.example.movieshibernatedb.services.CrudService;

import java.util.Collection;
import java.util.Set;

public interface FranchiseService extends CrudService<Franchise, Integer> {
    Collection <Movie> getMoviesFranchise(Franchise franchise);
    Collection <Character> getCharactersFranchise(Franchise franchise);
    void updateMoviesFranchise(int[] movieIds, int franchiseId);


}
