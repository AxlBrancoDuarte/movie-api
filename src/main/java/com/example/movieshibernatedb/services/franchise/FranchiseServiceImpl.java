package com.example.movieshibernatedb.services.franchise;
import com.example.movieshibernatedb.exceptions.NotFoundException;
import com.example.movieshibernatedb.models.domain.Character;
import com.example.movieshibernatedb.models.domain.Franchise;
import com.example.movieshibernatedb.models.domain.Movie;
import com.example.movieshibernatedb.repositories.CharacterRepository;
import com.example.movieshibernatedb.repositories.FranchiseRepository;
import com.example.movieshibernatedb.repositories.MovieRepository;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

@Service
public class FranchiseServiceImpl implements FranchiseService {

    private final FranchiseRepository franchiseRepository;
    private final CharacterRepository characterRepository;
    private final MovieRepository movieRepository;


    public FranchiseServiceImpl(FranchiseRepository franchiseRepository, CharacterRepository characterRepository, MovieRepository movieRepository, CharacterRepository characterRepository1, MovieRepository movieRepository1) {
        this.franchiseRepository = franchiseRepository;
        this.characterRepository = characterRepository1;
        this.movieRepository = movieRepository1;
    }

    @Override
    public Franchise findById(Integer id) {
        return franchiseRepository.findById(id).orElseThrow(() -> new NotFoundException("Franchise does not exist with ID: " + id));
    }

    @Override
    public Collection<Franchise> findAll() {
        return franchiseRepository.findAll();
    }

    @Override
    public Franchise add(Franchise entity) {
        return franchiseRepository.save(entity);
    }

    @Override
    public void update(Franchise franchise) {
        int id = franchise.getId();
        if(!franchiseRepository.existsById(id)) throw new NotFoundException("Franchise does not exist with ID: " + id);
        franchiseRepository.save(franchise);
    }

    @Override
    public void deleteById(Integer id) {
        Franchise franchise = franchiseRepository.findById(id).orElseThrow(() -> new NotFoundException("Franchise does not exist with ID: " + id));
        franchise.getMovies().forEach(m -> m.setFranchise(null));
        franchiseRepository.delete(franchise);
    }


    @Override
    public Collection<Movie> getMoviesFranchise(Franchise franchise) {
        return franchise.getMovies();
    }

    @Override
    @Transactional
    public Collection<Character> getCharactersFranchise(Franchise franchise) {
        Set<Character> characters = new HashSet<>();
        Set<Integer> charIds = franchiseRepository.getMovieCharIdsFranchise(franchise.getId());
        for (int id: charIds){
            Character character = characterRepository.findById(id).get();
            characters.add(character);
        }
        return characters;

    }

    @Override
    @Transactional
    public void updateMoviesFranchise(int[] movieIds, int franchiseId){
        Franchise franchise = franchiseRepository.findById(franchiseId).orElseThrow(() -> new NotFoundException("Franchise does not exist with ID: " + franchiseId));
        Set<Movie> oldMovies = franchise.getMovies();
        oldMovies.forEach(o -> o.setFranchise(null));
        for(Integer id: movieIds){
            Movie movie = movieRepository.findById(id).get();
            movie.setFranchise(franchise);
        }
    }

}

