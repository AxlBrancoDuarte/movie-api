package com.example.movieshibernatedb.services.character;

import com.example.movieshibernatedb.models.domain.Character;
import com.example.movieshibernatedb.services.CrudService;

public interface CharacterService extends CrudService<Character, Integer> {

}
