package com.example.movieshibernatedb.services.character;

import com.example.movieshibernatedb.exceptions.NotFoundException;
import com.example.movieshibernatedb.models.domain.Character;
import com.example.movieshibernatedb.repositories.CharacterRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.Collection;


@Service
public class CharacterServiceImpl implements CharacterService {
    private final CharacterRepository characterRepository;

    public CharacterServiceImpl(CharacterRepository characterRepository) {
        this.characterRepository = characterRepository;
    }

    @Override
    public Character findById(Integer id) {
        return characterRepository.findById(id).orElseThrow(() -> new NotFoundException("Character does not exist with ID: " + id));
    }

    @Override
    public Collection<Character> findAll() {
        return characterRepository.findAll();
    }

    @Override
    public Character add(Character entity) {
        return characterRepository.save(entity);
    }

    @Override
    public void update(Character entity) {
        characterRepository.save(entity);
    }

    @Override
    @Transactional
    public void deleteById(Integer id){
        Character character = characterRepository.findById(id).orElseThrow(() -> new NotFoundException("Character does not exits with ID: " + id));
        character.getMovies().forEach(m -> m.getCharacters().remove(character));
        characterRepository.delete(character);
    }

}
