package com.example.movieshibernatedb;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MoviesHibernateDbApplication {

    public static void main(String[] args) {
        SpringApplication.run(MoviesHibernateDbApplication.class, args);
    }

}
