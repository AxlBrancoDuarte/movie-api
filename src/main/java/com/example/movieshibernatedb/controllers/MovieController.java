package com.example.movieshibernatedb.controllers;

import com.example.movieshibernatedb.mappers.MovieMapper;

import com.example.movieshibernatedb.models.dto.MovieDTO;
import com.example.movieshibernatedb.services.movie.MovieService;
import com.example.movieshibernatedb.models.utils.ApiErrorResponse;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.ArraySchema;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;


import java.util.Collection;


@RestController
@RequestMapping(path = "api/v1/movies")
public class MovieController {

    private final MovieService movieService;
    private final MovieMapper movieMapper;

    public MovieController(MovieService movieService, MovieMapper movieMapper) {
        this.movieService = movieService;

        this.movieMapper = movieMapper;
    }

    @Operation(summary = "Returns all movies")
    @ApiResponses( value = {
            @ApiResponse(responseCode = "200",
                    description = "Movies successfully returned",
                    content = {@Content(mediaType = "application/json",
                            array = @ArraySchema(schema = @Schema(implementation = MovieDTO.class)))}),
            @ApiResponse(responseCode = "400",
                    description = "Malformed request",
                    content = {@Content(mediaType = "application/json",
                            schema = @Schema(implementation = ApiErrorResponse.class))}),
            @ApiResponse(responseCode = "500",
                    description = "Internal Server Error: possibly no movies in database",
                    content = {@Content(mediaType = "application/json",
                            schema = @Schema(implementation = ApiErrorResponse.class))})
    })
    @GetMapping
    public ResponseEntity<Collection<MovieDTO>> findAll(){
        return ResponseEntity.ok(movieMapper.movieToMovieDto(movieService.findAll()));
    }


    @Operation(summary = "Returns movie with id")
    @ApiResponses( value = {
            @ApiResponse(responseCode = "200",
                    description = "Movie successfully returned",
                    content = {@Content(mediaType = "application/json",
                            schema = @Schema(implementation = MovieDTO.class))}),
            @ApiResponse(responseCode = "400",
                    description = "Malformed request",
                    content = {@Content(mediaType = "application/json",
                            schema = @Schema(implementation = ApiErrorResponse.class))}),
            @ApiResponse(responseCode = "404",
                    description = "Could not return movie: no movie with this id",
                    content = {@Content(mediaType = "application/json",
                            schema = @Schema(implementation = ApiErrorResponse.class))})
    })
    @GetMapping("/{id}")
    public ResponseEntity<MovieDTO> findById(@PathVariable int id){
        MovieDTO movieDTO = movieMapper.movieToMovieDto(movieService.findById(id));
        return ResponseEntity.ok(movieDTO);
    }


    @Operation(summary = "Adds movie to database")
    @ApiResponses( value = {
            @ApiResponse(responseCode = "200",
                    description = "Movies successfully added to database",
                    content = {@Content(mediaType = "application/json",
                            schema = @Schema(implementation = MovieDTO.class))}),
            @ApiResponse(responseCode = "400",
                    description = "Malformed request",
                    content = {@Content(mediaType = "application/json",
                            schema = @Schema(implementation = ApiErrorResponse.class))})
    })
    @PostMapping()
    public ResponseEntity<MovieDTO> add(@RequestBody MovieDTO movieDTO){
        movieService.add(movieMapper.movieDtoToMovie(movieDTO));
        return ResponseEntity.ok(movieDTO);
    }


    @Operation(summary = "Updates a movie")
    @ApiResponses( value = {
            @ApiResponse(responseCode = "200",
                    description = "Movie successfully updated",
                    content = {@Content(mediaType = "application/json",
                            schema = @Schema(implementation = MovieDTO.class))}),
            @ApiResponse(responseCode = "400",
                    description = "Malformed request",
                    content = {@Content(mediaType = "application/json",
                            schema = @Schema(implementation = ApiErrorResponse.class))}),
            @ApiResponse(responseCode = "404",
                    description = "Movie not found with supplied ID",
                    content = {@Content(mediaType = "application/json",
                            schema = @Schema(implementation = ApiErrorResponse.class))})
    })
    @PutMapping("/{id}")
    public ResponseEntity<MovieDTO> update(@RequestBody MovieDTO movieDTO, @PathVariable int id){
        if(movieDTO.getId() != id)
            return ResponseEntity.badRequest().build();
        movieService.update(movieMapper.movieDtoToMovie(movieDTO)
        );
        return ResponseEntity.noContent().build();
    }

    @Operation(summary = "Deletes movie with id")
    @ApiResponses( value = {
            @ApiResponse(responseCode = "204",
                    description = "Movie successfully deleted",
                    content = @Content),
            @ApiResponse(responseCode = "400",
                    description = "Malformed request",
                    content = {@Content(mediaType = "application/json",
                            schema = @Schema(implementation = ApiErrorResponse.class))}),
            @ApiResponse(responseCode = "404",
                    description = "Could not delete movie: no movie with this id",
                    content = {@Content(mediaType = "application/json",
                            schema = @Schema(implementation = ApiErrorResponse.class))})
    })

    @DeleteMapping("/{id}")
    public void delete(@PathVariable int id){
        movieService.deleteById(id);
    }
}

