package com.example.movieshibernatedb.mappers;

import com.example.movieshibernatedb.models.domain.Franchise;
import com.example.movieshibernatedb.models.domain.Movie;
import com.example.movieshibernatedb.models.dto.FranchiseDTO;
import com.example.movieshibernatedb.services.movie.MovieService;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Named;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.Collection;
import java.util.Set;
import java.util.stream.Collectors;


@Mapper(componentModel = "spring")
public abstract class FranchiseMapper {
    @Autowired
    private MovieService movieService;

    @Mapping(target="movies", source="movies", qualifiedByName = "mapMoviesToDTO")
    public abstract FranchiseDTO franToFranDTO(Franchise franchise);

    @Mapping(target="movies", source="movies", qualifiedByName = "mapMoviesToDTO")
    public abstract Collection<FranchiseDTO> franToFranDTO(Collection<Franchise> franchises);

    @Mapping(target="movies", source="movies", qualifiedByName = "mapMoviesFromDTO")
    public abstract Franchise franDTOToFran(FranchiseDTO franchiseDTO);

    @Named("mapMoviesToDTO")
    Set<Integer> mapMoviesToDTO(Set<Movie> movies){
        if (movies == null) return null;
        return movies.stream().map(m -> m.getId()).collect(Collectors.toSet());
    }

    @Named("mapMoviesFromDTO")
    Set<Movie> mapMoviesFromDTO(Set<Integer> movieIds){
        if (movieIds == null) return null;
        return movieIds.stream().map(i -> movieService.findById(i)).collect(Collectors.toSet());
    }

}



