package com.example.movieshibernatedb.mappers;
import com.example.movieshibernatedb.models.domain.Character;
import com.example.movieshibernatedb.models.domain.Movie;
import com.example.movieshibernatedb.models.dto.CharacterDTO;
import com.example.movieshibernatedb.services.movie.MovieService;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Named;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.Collection;
import java.util.Set;
import java.util.stream.Collectors;



@Mapper(componentModel = "spring")
public abstract class CharacterMapper {
    @Autowired
    private MovieService movieService;


    @Mapping(target="movies", source="movies")
    public abstract CharacterDTO movieCharToCharDTO(Character character);

    @Mapping(target="movies", source="movies")
    public abstract Collection<CharacterDTO> movieCharToCharDTO(Collection<Character> characters);
    @Mapping(target="movies", source="movies", qualifiedByName = "mapToMovieCharacters")
    public abstract Character characterDTOToMovieCharacter(CharacterDTO characterDTO);

    @Named("mapToMovieCharacters")
    Set<Movie> mapToMovieCharacters(Set<Integer> movieIds){
        if(movieIds == null) return null;
        return movieIds.stream().map(m -> movieService.findById(m)).collect(Collectors.toSet());
    }
    Set<Integer> map(Set<Movie> movies){
        if (movies == null) return null;
        return movies.stream().map(m -> m.getId()).collect(Collectors.toSet());
    }

}


