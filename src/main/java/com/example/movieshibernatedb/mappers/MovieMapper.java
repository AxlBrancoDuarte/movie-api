package com.example.movieshibernatedb.mappers;

import com.example.movieshibernatedb.models.domain.Character;
import com.example.movieshibernatedb.models.domain.Franchise;
import com.example.movieshibernatedb.models.domain.Movie;
import com.example.movieshibernatedb.models.dto.MovieDTO;
import com.example.movieshibernatedb.services.character.CharacterService;
import com.example.movieshibernatedb.services.franchise.FranchiseService;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Named;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.Collection;
import java.util.Set;
import java.util.stream.Collectors;



@Mapper(componentModel = "spring")
public abstract class MovieMapper {
    @Autowired
    private CharacterService characterService;
    @Autowired
    private FranchiseService franchiseService;


    @Mapping(target="franchise", source="franchise", qualifiedByName = "mapFromFranchise")
    @Mapping(target="characters", source="characters", qualifiedByName = "mapFromCharacters")
    public abstract MovieDTO movieToMovieDto(Movie movie);

    @Mapping(target="franchise", source="franchise", qualifiedByName = "mapFromFranchise")
    @Mapping(target="characters", source="characters", qualifiedByName = "mapFromCharacters")
    public abstract Collection<MovieDTO> movieToMovieDto(Collection<Movie> movies);

    @Mapping(target="franchise", source="franchise", qualifiedByName = "mapToFranchise")
    @Mapping(target="characters", source="characters", qualifiedByName = "mapToCharacters")
    public abstract Movie movieDtoToMovie(MovieDTO movieDTO);



    @Named("mapToCharacters")
    Set<Character> mapToCharacters(Set<Integer> characterIds){
        if (characterIds == null) return null;
        return characterIds.stream().map(i -> characterService.findById(i)).collect(Collectors.toSet());
    }

    @Named("mapFromCharacters")
    Set<Integer> mapFromCharacters(Set<Character> characters){
        if (characters ==  null) return null;
        return characters.stream().map(Character::getId).collect(Collectors.toSet());
    }

    @Named("mapFromFranchise")
    Integer mapFromFranchise(Franchise franchise){
        if(franchise == null) return null;
        return franchise.getId();
    }

    @Named("mapToFranchise")
    Franchise mapToFranchise(Integer id){
        if(id == null) return null;
        return franchiseService.findById(id);
    }

}






